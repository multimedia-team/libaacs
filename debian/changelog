libaacs (0.11.1-4) unstable; urgency=medium

  * Team upload.

  [ Christoph Anton Mitterer ]
  * Update debian/copyright

  [ Dylan Aïssi ]
  * Remove m4/libgcrypt.m4 which ships an outdated version of
    AM_PATH_LIBGCRYPT() (Closes: #1071931)
  * Remove unneeded debian/salsa-ci.yml
  * Standards-Version: 4.7.0 (no changes required)

 -- Dylan Aïssi <daissi@debian.org>  Sat, 29 Jun 2024 08:49:50 +0200

libaacs (0.11.1-3) unstable; urgency=medium

  * Team upload.
  * Package aacs_info in a new libaacs-bin package. (Closes: #1056989)

 -- Christoph Anton Mitterer <mail@christoph.anton.mitterer.name>  Wed, 17 Jan 2024 05:45:47 +0100

libaacs (0.11.1-2) unstable; urgency=medium

  * Team upload.
  * libaacs0: enhances libbluray2 instead of libbluray1 (Closes: #1020911)
  * Standards-Version: 4.6.2 (no changes required)

 -- Dylan Aïssi <daissi@debian.org>  Fri, 10 Feb 2023 17:44:51 +0100

libaacs (0.11.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Standards-Version: 4.6.0 (no changes required)

 -- Dylan Aïssi <daissi@debian.org>  Fri, 04 Mar 2022 15:14:47 +0100

libaacs (0.11.0-2) unstable; urgency=medium

  * Team upload.
  * Bullseye has been released, so upload to unstable.

 -- Dylan Aïssi <daissi@debian.org>  Thu, 19 Aug 2021 09:11:46 +0200

libaacs (0.11.0-1) experimental; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set upstream metadata fields: Bug-Submit (from ./configure).
  * Update standards version to 4.2.1, no changes needed.

  [ Dylan Aïssi ]
  * New upstream release.
  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Add libaacs.a, libaacs.la and aacs_info in d/not-installed

 -- Dylan Aïssi <daissi@debian.org>  Mon, 08 Feb 2021 14:45:53 +0100

libaacs (0.9.0-2) unstable; urgency=medium

  * Team upload.

  [ Dylan Aïssi ]
  * Remove myself from Uploaders.
  * Fix typo in Reinhard's email address (Closes: #905847).
  * Bump Standards to 4.2.0.
  * Update watch file with secure URI.

 -- Dylan Aïssi <daissi@debian.org>  Sat, 08 Dec 2018 17:44:49 +0100

libaacs (0.9.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control: Switch to libgcrypt20-dev from libgcrypt11-dev.
      (Closes: #864110)
  * debian/{control,copyright,watch}: Switch to https.
  * Update debian/copyright.
  * Bump Standards to 4.0.0.
  * Bump dh_makeshlibs version to >= 0.9.0.

 -- Dylan Aïssi <bob.dybian@gmail.com>  Fri, 30 Jun 2017 23:33:34 +0200

libaacs (0.8.1-2) unstable; urgency=medium

  [ Alessio Treglia ]
  - Remove myself from Uploaders.

  [ Dylan Aïssi ]
  * debian/control:
    - Switch to secure URI.
    - Bump Standards to 3.9.8.

  [ Sebastian Ramacher ]
  * debian/control: Fix section of libaacs0.
  * debian/{rules,compat,control}: Bump debhelper compat to 10.
  * debian/rules: Remove useless CFLAGS mangling.

 -- Dylan Aïssi <bob.dybian@gmail.com>  Mon, 10 Oct 2016 20:56:04 +0200

libaacs (0.8.1-1) unstable; urgency=medium

  [ Rico Tzschichholz ]
  * New upstream release: 0.8.0
    - Add improved file system interface
    - Support opening raw devices
    - Reduce log level of failed PMSN query
    - Optimizations

  [ Dylan Aïssi ]
  * New upstream release: 0.8.1 (Closes: #731500)
  * debian/control:
    - Bump Standards to 3.9.6.
    - Add Recommends: libbdplus0.
    - Remove Andres Mejia from Uploaders (Closes: #743548)
    - Add myself to Uploaders.
  * debian/copyright: Update copyright.
  * debian/watch: Fix.

  [ Sebastian Ramacher ]
  * debian/source/lintian-overrides: Removed, no longer needed.

 -- Dylan Aïssi <bob.dybian@gmail.com>  Thu, 23 Jul 2015 17:44:36 +0200

libaacs (0.7.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Add support for gcrypt 1.6.0.
    - Do not retrieve MKB using MMC commands during AACS-Auth

 -- Rico Tzschichholz <ricotz@ubuntu.com>  Tue, 01 Jul 2014 19:17:19 +0200

libaacs (0.7.0-1) unstable; urgency=low

  [ Rico Tzschichholz ]
  * New upstream release.
    - Add aacs_get_device_binding_id() and aacs_get_device_nonce().
    - Add aacs_get_mk().
    - Add support for bus encryption.
    - Add support for device keys.
    - Add support for internal keys.
    - Fix reading PMSN.
    - Fix reporting AACS version from AACS drive feature.
    - Calculate bus key and use it to verify message authentication codes.
    - Do not fail if VID is known but config file does not exist.
    - Improve validating keys in KEYDB.cfg.
    - Update KEYDB.cfg format (still backwards compatible):
      * Host nonce and host key point are optional.
      * Add device key UV values and masks.
    - Build system updates.
    - Drop support for compile-time PATCHED_DRIVE flag.

  [ Alessio Treglia ]
  * Bump Standards.

 -- Alessio Treglia <alessio@debian.org>  Mon, 06 Jan 2014 09:35:53 +0000

libaacs (0.6.0-2) unstable; urgency=low

  * Upload to unstable.

 -- Alessio Treglia <alessio@debian.org>  Mon, 06 May 2013 11:28:07 +0200

libaacs (0.6.0-1) experimental; urgency=low

  * New upstream release.
  * Bump debhelper requirements.
  * Remove get-git-source.sh as it's no longer needed.
  * Remove patches, applied upstream.
  * Update debian/copyright.
  * Bump Standards.

 -- Alessio Treglia <alessio@debian.org>  Wed, 06 Mar 2013 16:43:47 +0000

libaacs (0.5.0-2) experimental; urgency=low

  * fix embarassing typo

 -- Reinhard Tartler <siretart@tauware.de>  Thu, 20 Sep 2012 20:02:05 +0200

libaacs (0.5.0-1) experimental; urgency=low

  [ Rico Tzschichholz ]
  * New upstream release:
    - Support for Mac OS X using IOKit
    - Fix AACS detection failure in some Win32 systems

  [ Reinhard Tartler ]
  * upload to experimental
  * bump shlibs following upstream minor version bump
  * add myself to uploaders

 -- Reinhard Tartler <siretart@tauware.de>  Thu, 20 Sep 2012 19:36:19 +0200

libaacs (0.4.0-1) unstable; urgency=low

  * New upstream release:
    - Added aacs_open2() and error codes
    - Renamed libaacs_test to aacs_info
    - Added aacs_get_mkb_version()
    - Fixed memory leaks
    - Fixed buffer overflow
    - Added revocation list caching
    - Check host certificates against latest seen revocation list

 -- Alessio Treglia <alessio@debian.org>  Mon, 07 May 2012 12:26:01 -0700

libaacs (0.3.1-1) unstable; urgency=low

  [ Alessio Treglia ]
  * New upstream release.
  * Refresh debian/copyright.
  * Bump Standards-Version to 3.9.3.

  [ Andres Mejia ]
  * Remove patches. Changes from patches applied upstream.
  * Add lintian override for
    libaacs source: package-needs-versioned-debhelper-build-depends 9.
  * Add myself to Uploaders field.
  * Don't disable optimizations.
  * Enable hardened flags without overriding -O3.
  * Remove the symbols file.
  * Remove debug packages since optimizations enable -fomit-frame-pointer.
  * Remove unnecessary override for dh_auto_clean.

 -- Andres Mejia <amejia@debian.org>  Wed, 21 Mar 2012 15:19:44 -0400

libaacs (0.3.0-4) unstable; urgency=low

  * Introduce Multi-Arch support.

 -- Alessio Treglia <alessio@debian.org>  Mon, 13 Feb 2012 23:35:15 +0100

libaacs (0.3.0-3) unstable; urgency=low

  * Disable default optimizations.
  * Don't override CFLAGS in src/Makefile.am.

 -- Alessio Treglia <alessio@debian.org>  Fri, 13 Jan 2012 12:44:45 +0100

libaacs (0.3.0-2) unstable; urgency=low

  [ Alessio Treglia ]
  * debian/control: package's long description cleanup (Closes: #652237).
  * Fix segfaults when mount point patch cannot be resolved.
    - The fix works on Linux only, since non-Linux archs still rely on
      the old code (cause of PATH_MAX).

  [ Rico Tzschichholz ]
  * Add debug package.

 -- Alessio Treglia <alessio@debian.org>  Thu, 12 Jan 2012 19:26:27 +0100

libaacs (0.3.0-1) unstable; urgency=low

  * First upstream stable release.
  * debian/libaacs0.docs: README.txt is no longer provided.
  * Update symbols file.
  * Point watch file to the project's FTP archive.

 -- Alessio Treglia <alessio@debian.org>  Sun, 04 Dec 2011 14:42:45 +0100

libaacs (0~20111107.gitc5f01a9-1) unstable; urgency=low

  [ Alessio Treglia ]
  * Imported Upstream snapshot 0~20111107.gitc5f01a9.
  * Add Enhances: libbluray1 on libaacs0.
  * Update symbols file.

  [ Andres Mejia ]
  * Fix build deps. libaacs needs just one 'yacc' implementation.

 -- Alessio Treglia <alessio@debian.org>  Tue, 08 Nov 2011 16:08:17 +0100

libaacs (0~20110623.git964342f-1) unstable; urgency=low

  * Initial release. (Closes: #637422)

 -- Alessio Treglia <alessio@debian.org>  Thu, 11 Aug 2011 11:34:32 +0200
